public class Foot {
    private FootShape shape;

    public Foot() {
        this.shape = new FootShape();
    }

    public String draw() {
        return "Drawing a foot";
    }
}
