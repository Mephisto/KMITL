public class FootShape {
    public String drawAsEllipse() {
        Ellipse ellipse = new Ellipse();
        return ellipse.draw();
    }

    public String drawAsRectangle() {
        Rectangle rectangle = new Rectangle();
        return rectangle.draw();
    }
}
