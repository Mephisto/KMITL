public interface IMathOperation {
    int calculate(int num1, int num2);
}
