interface ScoreObserver {
    void update(String scoreData);
}
